#include "websocket_client_tls.h"

namespace {
    std::shared_ptr<spdlog::logger> logger;
}

WebsocketClientTls::WebsocketClientTls(asio::io_service& ioService, spdlog::level::level_enum logLevel, bool logToStdError) :
    WebsocketClientBase(ioService, logLevel, logToStdError),
    ioService_(ioService)
{
    logger = logToStdError ? spdlog::stderr_logger_mt("WebsocketClientTls") : spdlog::stdout_logger_mt("WebsocketClientTls");
    spdlog::set_level(logLevel);

    logger->trace("WebsocketClientTls()");
    init();
}

void WebsocketClientTls::close()
{
    logger->trace("close");
    websocketpp::lib::error_code ec;
    client_.close(handle_, websocketpp::close::status::going_away, "", ec);
}

void WebsocketClientTls::onOpen(websocketpp::connection_hdl hdl)
{
    logger->trace("onOpen");
    handle_ = hdl;

    Client::connection_ptr con = client_.get_con_from_hdl(hdl);
    serverId_ = con->get_response_header("Server");
    logger->debug("Connection opened to {}", serverId_);
    openCb(serverId_);
}

void WebsocketClientTls::onFail(websocketpp::connection_hdl hdl)
{
    logger->trace("onFail");

    Client::connection_ptr con = client_.get_con_from_hdl(hdl);
    serverId_ = con->get_response_header("Server");
    logger->debug("Connection failed: {}", con->get_ec().message());
    failCb(serverId_, con->get_ec().message());
}

void WebsocketClientTls::onClosed(websocketpp::connection_hdl hdl)
{
    Client::connection_ptr con = client_.get_con_from_hdl(hdl);
    serverId_ = con->get_response_header("Server");

    logger->debug("Connection closed: {0} ({1}) reason:{2}",
                 websocketpp::close::status::get_string(con->get_remote_close_code()),
                 con->get_remote_close_code(), con->get_remote_close_reason());
    closedCb(serverId_, con->get_remote_close_reason());
}

void WebsocketClientTls::onMessage(websocketpp::connection_hdl handle, Client::message_ptr msg)
{
    logger->debug("Message from {0}: {1}", remoteEndpoint(handle), msg->get_payload());
    messageCb(msg->get_payload());
}

bool WebsocketClientTls::connect(const std::string& uri)
{
    websocketpp::lib::error_code ec;
    Client::connection_ptr con = client_.get_connection(uri, ec);

    if (ec)
    {
        logger->error("Connect failed to create connection: {0}", ec.message());
        onFail(websocketpp::connection_hdl());
        return false;
    }

    handle_ = con->get_handle();

    con->set_open_handler(std::bind(&WebsocketClientTls::onOpen, this, std::placeholders::_1));
    con->set_fail_handler(std::bind(&WebsocketClientTls::onFail, this, std::placeholders::_1));
    con->set_close_handler(std::bind(&WebsocketClientTls::onClosed, this, std::placeholders::_1));
    con->set_message_handler(std::bind(&WebsocketClientTls::onMessage, this, std::placeholders::_1, std::placeholders::_2));

    client_.connect(con);

    return true;
}

void WebsocketClientTls::send(const std::string& msg)
{
    logger->trace("send:{}", msg);
    try
    {
        client_.send(handle_, msg, websocketpp::frame::opcode::text);
    }
    catch (const std::exception& e)
    {
        logger->error("send:Caught exception: {}", e.what());
    }
}

void WebsocketClientTls::init()
{
    client_.clear_access_channels(websocketpp::log::alevel::all);
    client_.clear_error_channels(websocketpp::log::elevel::all);
    client_.set_tls_init_handler([this](websocketpp::connection_hdl)
        {
            websocketpp::lib::shared_ptr<asio::ssl::context> ctx = websocketpp::lib::make_shared<asio::ssl::context>(asio::ssl::context::tlsv1);
            try
            {
                ctx->set_options(asio::ssl::context::verify_none |
                                 asio::ssl::context::default_workarounds |
                                 asio::ssl::context::no_sslv2 |
                                 asio::ssl::context::no_sslv3 |
                                 asio::ssl::context::single_dh_use);
            }
            catch (const std::exception& e)
            {
                logger->error("init:Caught exception {}", e.what());
            }
            return ctx;
        });

    client_.init_asio(&ioService_);
}

std::string WebsocketClientTls::remoteEndpoint(websocketpp::connection_hdl handle)
{
    Client::connection_ptr connection = client_.get_con_from_hdl(handle);
    return connection ? connection->get_remote_endpoint() : "";
}
