#ifndef webserver_INCLUDED
#define webserver_INCLUDED

#include <stdint.h>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <unordered_set>
#include <functional>
#include <tuple>
#include <thread>
#include <boost/asio.hpp>
#include <spdlog/spdlog.h>

#include "websocket_server.h"
#include "tqueue.h"

class WebServer
{
public:

    typedef WebsocketServer::ConnectionPtr ConnectionPtr;

    struct Request
    {
        enum class Type
        {
            Connect = 0,
            Disconnect,
            Http,
            Websocket
        };

        Request(Type requestType, ConnectionPtr conn, const std::string& route, const std::string& remote,
                const std::string& uri, const std::string& method, const std::map<std::string,std::string>& query, const std::string& msg) :
            requestType(requestType), conn(conn), route(route), remote(remote), uri(uri), method(method), query(query), msg(msg)
        {}

        // websocket msg request
        Request(ConnectionPtr conn, const std::string& msg) : requestType(Type::Websocket), conn(conn), msg(msg)
        {}

        Type requestType;
        ConnectionPtr conn;
        std::string route;
        std::string remote;
        std::string uri;
        std::string method;
        std::map<std::string,std::string> query;
        std::string msg;
    };
    typedef std::shared_ptr<Request> RequestPtr;
    typedef WebsocketServer::Response Response;
    typedef WebsocketServer::ResponsePtr ResponsePtr;

    typedef std::function<bool(RequestPtr request, ResponsePtr response)> WebsocketMessageFn;
    typedef std::function<bool(RequestPtr request, ResponsePtr response)> HttpReqFn;
    typedef std::function<void(bool connected, RequestPtr request)> WebsocketConnectFn;

    struct RouteSpec
    {
        RouteSpec(const std::string& path = "") : path(path) {}

        static RouteSpec create(const std::string& path = "") { return RouteSpec(path); }

        RouteSpec& setMethods(const std::unordered_set<std::string>& methods) { this->methods = methods; return *this; }
        RouteSpec& addMethod(const std::string& method) { methods.insert(method); return *this; }

        RouteSpec& setWebsocketMessageCb(const WebsocketMessageFn& cb) { this->websocketMessageCb = cb; return *this; }

        RouteSpec& setHttpRequestCb(const HttpReqFn& cb) { this->httpReqCb = cb; return *this; }

        RouteSpec& setWebsocketConnectCb(const WebsocketConnectFn& cb) { this->websocketConnectCb = cb; return *this; }

        std::string path;
        std::unordered_set<std::string> methods;
        WebsocketMessageFn websocketMessageCb;
        HttpReqFn httpReqCb;
        WebsocketConnectFn websocketConnectCb;
    };

    WebServer(boost::asio::io_service& ioService, uint16_t port);

    void startListening(uint16_t port);
    void sendToAll(const std::string& msg);
    void send(const std::string& remoteAddress, const std::string& msg);

    static void sendHttpResponse(ConnectionPtr conn, ResponsePtr response) { WebsocketServer::sendHttpResponse(conn, response); }
    static void sendWebsocketResponse(ConnectionPtr conn, ResponsePtr response) { WebsocketServer::sendWebsocketResponse(conn, response); }

    std::vector<std::string> getConnectedClients();
    std::size_t getConnectedClientCount() const;

    void addRoute(const RouteSpec& route);

    void setLogLevel(spdlog::level::level_enum level);

private:

    /// Set of assigned routes, (the std::function is the sorting comparator)
    typedef std::set<RouteSpec, std::function<bool(const RouteSpec&, const RouteSpec&)>> Routes;

    static bool routeMatch(const std::string& uri, const std::string& routeSpec);
    const RouteSpec& routeGet(const std::string& uri, const std::string& method) const;

    void processRequest(const RequestPtr request);
    void websocketConnectCb(WebsocketServer::ConnectionPtr conn, const std::string& remote, const std::string& uri, const std::string& method);
    void websocketDisconnectCb(WebsocketServer::ConnectionPtr conn, const std::string& remote, const std::string& uri, const std::string& method);

    void websocketCb(
            WebsocketServer::ConnectionPtr conn, const std::string& remote, const std::string& uri, const std::string& method, const std::string& msg);
    void httpCb(
            WebsocketServer::ConnectionPtr conn, const std::string& remote, const std::string& uri, const std::string& method, const std::string& msg);

    WebsocketServer websocketServer_;
    Routes routes_;
    std::map<WebsocketServer::ConnectionPtr, RouteSpec> websocketRouteMap_;

    std::vector<std::thread> workers_;
//    TQueue<RequestPtr> requests_;

    std::shared_ptr<spdlog::logger> logger;

    // Route spec comparitor function - sorts longest strings first
    static std::function<bool(const WebServer::RouteSpec& lhs, const WebServer::RouteSpec& rhs)> RouteSpecCmp;
};

#endif // ifndef webserver_INCLUDED
