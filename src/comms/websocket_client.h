#ifndef websocket_client_INCLUDED
#define websocket_client_INCLUDED

#include <stdint.h>
#include <string>
#include <vector>
#include <functional>
#include <set>
#include <tuple>

#include <asio.hpp>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/client.hpp>
#include <spdlog/spdlog.h>
#include "websocket_client_base.h"

class WebsocketClient : public WebsocketClientBase
{
public:

    WebsocketClient(asio::io_service& ioService, spdlog::level::level_enum logLevel, bool logToStdError = false);
    ~WebsocketClient() {}

    bool connect(const std::string& uri) override;
    void send(const std::string& msg) override;
    void close() override;

    void setOpenCb(std::function<void(const std::string& serverId)> openCb)  override { openCb_ = openCb; }
    void setFailCb(std::function<void(const std::string& serverId, const std::string& error)> failCb)  override { failCb_ = failCb; }
    void setClosedCb(std::function<void(const std::string& serverId, const std::string& error)> closedCb)  override { closedCb_ = closedCb; }
    void setMessageCb(std::function<void(const std::string&)> messageCb) override { messageCb_ = messageCb; }

private:
    typedef websocketpp::client<websocketpp::config::asio> Client;

    void init();
    std::string remoteEndpoint(websocketpp::connection_hdl handle);
    void onOpen(websocketpp::connection_hdl hdl);
    void onFail(websocketpp::connection_hdl hdl);
    void onClosed(websocketpp::connection_hdl hdl);
    void onMessage(websocketpp::connection_hdl handle, Client::message_ptr msg);

    void openCb(const std::string& serverId) { if (openCb_) openCb_(serverId); }
    void failCb(const std::string& serverId, const std::string& error) { if (failCb_) failCb_(serverId, error); }
    void closedCb(const std::string& serverId, const std::string& error) { if (closedCb_) closedCb_(serverId, error); }
    void messageCb(const std::string& msg) { if (messageCb_) messageCb_(msg); }

    asio::io_service& ioService_;
    Client client_;

    websocketpp::connection_hdl handle_;

    std::function<void(const std::string& serverId)> openCb_;
    std::function<void(const std::string& serverId, const std::string& error)> failCb_;
    std::function<void(const std::string& serverId, const std::string& error)> closedCb_;
    std::function<void(const std::string&)> messageCb_;
    std::function<std::tuple<websocketpp::http::status_code::value, std::string> (const std::string&)> httpMessageCb_;
    std::string serverId_;
};

#endif // ifndef websocket_client_INCLUDED
