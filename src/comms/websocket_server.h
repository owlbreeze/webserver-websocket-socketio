#ifndef websocket_server_INCLUDED
#define websocket_server_INCLUDED

#include <stdint.h>
#include <string>
#include <vector>
#include <functional>
#include <set>
#include <tuple>
#include <thread>

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>
#include <boost/asio.hpp>
#include <spdlog/spdlog.h>

class WebsocketServer
{
public:
//    class persistent_config : public websocketpp::config::asio
//    {
//    public:
//        static bool const enable_persistent_connections = true;
//    };

    typedef websocketpp::server<websocketpp::config::asio> Server;
//    typedef websocketpp::server<persistent_config> Server;

    typedef Server::connection_ptr ConnectionPtr;

    struct Response
    {
        Response() : httpStatus(websocketpp::http::status_code::internal_server_error), websocketOpCode(websocketpp::frame::opcode::text) {}

        Response(const std::string& content, const std::vector<std::pair<std::string, std::string>>& headers, websocketpp::http::status_code::value httpStatus) :
            httpStatus(httpStatus),
            websocketOpCode(websocketpp::frame::opcode::text),
            content(content),
            headers(headers)
        {}

        Response(const std::string& content, websocketpp::frame::opcode::value websocketOpCode = websocketpp::frame::opcode::text) :
            httpStatus(websocketpp::http::status_code::internal_server_error),
            websocketOpCode(websocketOpCode),
            content(content)
        {}

        Response& setHttpStatus(websocketpp::http::status_code::value status)
        {
            httpStatus = status;
            return *this;
        }

        Response& setContent(const std::string& data)
        {
            content = data;
            return *this;
        }

        Response& addHeader(const std::string& key, const std::string& value)
        {
            headers.push_back({key,value});
            return *this;
        }

        websocketpp::http::status_code::value httpStatus;
        websocketpp::frame::opcode::value websocketOpCode;
        std::string content;
        std::vector<std::pair<std::string, std::string>> headers;
    };

    typedef std::shared_ptr<Response> ResponsePtr;

    typedef std::function<void
            (ConnectionPtr conn, const std::string& remote, const std::string& uri, const std::string& method, const std::string& msg)> WebsocketMessageFn;

    typedef std::function<void
            (ConnectionPtr conn, const std::string& remote, const std::string& uri, const std::string& method, const std::string& msg)> HttpReqFn;

    typedef std::function<void
            (ConnectionPtr conn, const std::string& remote, const std::string& uri, const std::string& method)> WebsocketConnectFn;

    typedef std::function<void
            (ConnectionPtr conn, const std::string& remote, const std::string& uri, const std::string& method)> WebsocketDisconnectFn;


    WebsocketServer(boost::asio::io_service& ioService);
    WebsocketServer(boost::asio::io_service& ioService, uint16_t port);

    void startListening(uint16_t port);
    void sendToAll(const std::string& msg);
    void send(const std::string& remoteAddress, const std::string& msg);

    /// Send a deferred response.
    static void sendHttpResponse(ConnectionPtr conn, ResponsePtr response);
    static void sendWebsocketResponse(ConnectionPtr conn, ResponsePtr response);

    std::vector<std::string> getConnectedClients();
    std::size_t getConnectedClientCount() const;

    void setWebsocketConnectCb(WebsocketConnectFn cb) { connectCb_ = cb; }
    void setWebsocketDisconnectCb(WebsocketDisconnectFn cb) { disconnectCb_ = cb; }
    void setWebsocketMessageCb(WebsocketMessageFn wsMessageCb) { wsMessageCb_ = wsMessageCb; }
    void setHttpMessageCb(HttpReqFn httpMessageCb) { httpMessageCb_ = httpMessageCb; }

    void setLogLevel(spdlog::level::level_enum level);

private:

    void init();
    std::string remoteEndpoint(websocketpp::connection_hdl handle);
    std::string remoteEndpoint(Server::connection_ptr connection);
    void onSocketOpen(websocketpp::connection_hdl handle);
    void onSocketClose(websocketpp::connection_hdl handle);
    void onHttp(websocketpp::connection_hdl handle);
    void onMessage(websocketpp::connection_hdl handle, Server::message_ptr msg);

    /// Safely get a copy of the connections set.
    std::set<websocketpp::connection_hdl, std::owner_less<websocketpp::connection_hdl>> connections()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        return connections_;
    }

    boost::asio::io_service& ioService_;
    Server server_;
    std::set<websocketpp::connection_hdl, std::owner_less<websocketpp::connection_hdl>> connections_;
    mutable std::mutex mutex_;
    WebsocketMessageFn wsMessageCb_;
    HttpReqFn httpMessageCb_;
    WebsocketConnectFn connectCb_;
    WebsocketDisconnectFn disconnectCb_;
    std::shared_ptr<spdlog::logger> logger;
};

#endif // ifndef websocket_server_INCLUDED
