#include "webserver.h"
#include "wildcard.h"
#include "common/url.hh"

using std::string;

WebServer::WebServer(boost::asio::io_service& ioService, uint16_t port) :
    websocketServer_(ioService, port),
    routes_(RouteSpecCmp)
{
    std::ostringstream oss;
    oss << "WebServer-" << port;
    logger = spdlog::stdout_logger_mt(oss.str());

    websocketServer_.setWebsocketMessageCb(
                std::bind(&WebServer::websocketCb, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5));

    websocketServer_.setHttpMessageCb(
                std::bind(&WebServer::httpCb, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5));

    websocketServer_.setWebsocketConnectCb(
                std::bind(&WebServer::websocketConnectCb, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
    websocketServer_.setWebsocketDisconnectCb(
                std::bind(&WebServer::websocketDisconnectCb, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));

    logger->info() << "WebServer()";
}

void WebServer::processRequest(RequestPtr requestPtr)
{
    logger->debug("processRequest enter");

    const Request& request = *requestPtr;
    const WebServer::RouteSpec& route = routeGet(request.uri/*urlParsed.path()*/, request.method);

    //Response response;
    auto response = std::make_shared<Response>();

    if (request.requestType == Request::Type::Http && route.httpReqCb)
    {
        if (route.httpReqCb(requestPtr, response))
            sendHttpResponse(request.conn, response);
    }
    else if (request.requestType == Request::Type::Websocket && route.websocketMessageCb)
    {
        if (route.websocketMessageCb(requestPtr, response))
            sendWebsocketResponse(request.conn, response);
    }
    else if (request.requestType == Request::Type::Http)    // no route for this http request
    {
        response->setHttpStatus(websocketpp::http::status_code::not_found);
        sendHttpResponse(request.conn, response);
    }
    logger->trace("processRequest enter");
}

bool WebServer::routeMatch(const string& uri, const string& routeSpec)
{
    static auto removeLeadingTrailingSlashes = [](const string& srcStr)
        {
            string str = srcStr;
            while (!str.empty() && str.back() == '/')
                str.pop_back();
            while (!str.empty() && str[0] == '/')
                str.erase(str.begin(), str.begin()+1);
            return str;
        };

    return wildcard_compare(removeLeadingTrailingSlashes(routeSpec).c_str(),
                            removeLeadingTrailingSlashes(uri).c_str());
}

void WebServer::websocketConnectCb(ConnectionPtr conn, const string& remote, const string& uri, const string& method)
{
    Url urlParsed = Url().parsePathAndQuery(uri);

    logger->debug("websocketConnectCb: remote:{0}, uri:{1}, method:{2}", remote, uri, method);

    const WebServer::RouteSpec& route = routeGet(urlParsed.path(), method);

    websocketRouteMap_.insert(std::make_pair(conn, route));

    if (route.websocketConnectCb)
    {
        route.websocketConnectCb(true, std::make_shared<Request>(Request::Type::Connect, conn, "", remote, urlParsed.path(), method, urlParsed.queryParams(), ""));// RequestPtr(new Request(Request::Type::Connect, conn, "", remote, urlParsed.path(), method, urlParsed.queryParams(), "")));
    }
}

void WebServer::websocketDisconnectCb(ConnectionPtr conn, const string& remote, const string& uri, const string& method)
{
    Url urlParsed = Url().parsePathAndQuery(uri);

    logger->debug("websocketDisconnectCb: remote:{0}, uri:{1}, method:{2}", remote, uri, method);

    const WebServer::RouteSpec& route = routeGet(urlParsed.path(), method);
    if (route.websocketConnectCb)
    {
        route.websocketConnectCb(false, std::make_shared<Request>(Request::Type::Connect, conn, "", remote, urlParsed.path(), method, urlParsed.queryParams(), ""));// RequestPtr(new Request(Request::Type::Connect, conn, "", remote, urlParsed.path(), method, urlParsed.queryParams(), "")));
    }

    auto it = websocketRouteMap_.find(conn);
    if (it != websocketRouteMap_.end())
        websocketRouteMap_.erase(it);
}

void WebServer::websocketCb(
        WebsocketServer::ConnectionPtr conn, const string& remote, const string& uri, const string& method, const string& msg)
{
    logger->debug("websocketCb: remote:{0}, uri:{1}, method:{2}, msg:{3}", remote, uri, method, msg);

    //Response response;
    auto response = std::make_shared<Response>();

    auto it = websocketRouteMap_.find(conn);
    if (it != websocketRouteMap_.end())
    {
        RouteSpec& route = (*it).second;
        if (route.websocketMessageCb(std::make_shared<Request>(conn, msg), response))
            sendWebsocketResponse(conn, response);
    }
}

void WebServer::httpCb(
        WebsocketServer::ConnectionPtr conn, const string& remote, const string& uri, const string& method, const string& msg)
{
    // separate uri path & query params
    Url urlParsed = Url().parsePathAndQuery(uri);

    logger->debug("httpCb: remote:{0}, uri:{1}, method:{2}, msg:{3}", remote, urlParsed.path(), method, msg);

    processRequest(std::make_shared<Request>(Request::Type::Http, conn, "", remote, urlParsed.path(), method, urlParsed.queryParams(), msg));
}

const WebServer::RouteSpec& WebServer::routeGet(const string& uri, const string& method) const
{
    for (const RouteSpec & route : routes_)
    {
        if (routeMatch(uri, route.path))
        {
            logger->debug("routeGet: match:{} to spec:{}", uri, route.path);
            if (route.methods.size())
            {
                auto it = route.methods.find(method);
                if (it != route.methods.end())
                    return route;
                continue;
            }
            return route;
        }
    }
    static RouteSpec emptyRouteSpec;
    return emptyRouteSpec;
}

void WebServer::addRoute(const RouteSpec& route)
{
    routes_.insert(route);
}

void WebServer::setLogLevel(spdlog::level::level_enum level)
{
    logger->set_level(level);
}

std::vector<std::string> WebServer::getConnectedClients()
{
    return websocketServer_.getConnectedClients();
}

std::size_t WebServer::getConnectedClientCount() const
{
    return websocketServer_.getConnectedClientCount();
}

// sort longest strings first
// std::set uses the comparator to know if an element is the same as existing - tests !(a < b) && !(b < a)
std::function<bool(const WebServer::RouteSpec& lhs, const WebServer::RouteSpec& rhs)> WebServer::RouteSpecCmp =
    [](const WebServer::RouteSpec& lhs, const WebServer::RouteSpec& rhs)
    {
        if (lhs.path.size() > rhs.path.size())
            return true;

        if (lhs.path.size() < rhs.path.size())
            return false;

        return lhs.path > rhs.path;
    };
