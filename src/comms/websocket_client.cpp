#include "websocket_client.h"

namespace {
    std::shared_ptr<spdlog::logger> logger;
}

WebsocketClient::WebsocketClient(asio::io_service& ioService, spdlog::level::level_enum logLevel, bool logToStdError) :
    WebsocketClientBase(ioService, logLevel, logToStdError),
    ioService_(ioService)
{
    logger = logToStdError ? spdlog::stderr_logger_mt("WebsocketClient") : spdlog::stdout_logger_mt("WebsocketClient");
    spdlog::set_level(logLevel);

    logger->trace("WebsocketClient()");
    init();
}

void WebsocketClient::close()
{
    logger->trace("close");
    websocketpp::lib::error_code ec;
    client_.close(handle_, websocketpp::close::status::going_away, "", ec);
}

void WebsocketClient::onOpen(websocketpp::connection_hdl hdl)
{
    logger->trace("onOpen");
    handle_ = hdl;

    serverId_ = remoteEndpoint(hdl);
    logger->debug("Connection opened to {}", serverId_);
    openCb(serverId_);
}

void WebsocketClient::onFail(websocketpp::connection_hdl hdl)
{
    logger->trace("onFail");

    Client::connection_ptr con = client_.get_con_from_hdl(hdl);
    serverId_ = con->get_response_header("Server");
    logger->debug("Connection failed: {}", con->get_ec().message());
    failCb(serverId_, con->get_ec().message());
}

void WebsocketClient::onClosed(websocketpp::connection_hdl hdl)
{
    Client::connection_ptr con = client_.get_con_from_hdl(hdl);
    serverId_ = con->get_response_header("Server");

    logger->debug("Connection closed: {0} ({1}) reason:{2}",
                 websocketpp::close::status::get_string(con->get_remote_close_code()),
                 con->get_remote_close_code(), con->get_remote_close_reason());
    closedCb(serverId_, con->get_remote_close_reason());
}

void WebsocketClient::onMessage(websocketpp::connection_hdl handle, Client::message_ptr msg)
{
    logger->debug("Message from {0}: {1}", remoteEndpoint(handle), msg->get_payload());
    messageCb(msg->get_payload());
}

bool WebsocketClient::connect(const std::string& uri)
{
    websocketpp::lib::error_code ec;
    Client::connection_ptr con = client_.get_connection(uri, ec);

    if (ec)
    {
        logger->error("Connect failed to create connection to: {} : {}", uri, ec.message());
        //throw std::runtime_error(ec.message());
        onFail(websocketpp::connection_hdl());
        return false;
    }

    handle_ = con->get_handle();

    con->set_open_handler(std::bind(&WebsocketClient::onOpen, this, std::placeholders::_1));
    con->set_fail_handler(std::bind(&WebsocketClient::onFail, this, std::placeholders::_1));
    con->set_close_handler(std::bind(&WebsocketClient::onClosed, this, std::placeholders::_1));
    con->set_message_handler(std::bind(&WebsocketClient::onMessage, this, std::placeholders::_1, std::placeholders::_2));

    client_.connect(con);

    return true;
}

void WebsocketClient::send(const std::string& msg)
{
    logger->trace("send:{}", msg);
    try
    {
        client_.send(handle_, msg, websocketpp::frame::opcode::text);
    }
    catch (const std::exception& e)
    {
        logger->error("send:Caught exception: {}", e.what());
    }
}

void WebsocketClient::init()
{
    client_.clear_access_channels(websocketpp::log::alevel::all);
    client_.clear_error_channels(websocketpp::log::elevel::all);

    client_.init_asio(&ioService_);
}

std::string WebsocketClient::remoteEndpoint(websocketpp::connection_hdl handle)
{
    Client::connection_ptr connection = client_.get_con_from_hdl(handle);
    return connection ? connection->get_remote_endpoint() : "";
}
