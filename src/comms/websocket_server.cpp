#include "websocket_server.h"
#include "spdlog/spdlog.h"

using std::string;

namespace {
    auto logger = spdlog::stdout_logger_mt("WebsocketServer");
}

WebsocketServer::WebsocketServer(boost::asio::io_service& ioService) :
    ioService_(ioService)
{
    auto threadId = std::this_thread::get_id();
    std::ostringstream oss;
    oss << "WebsocketServer-[" << threadId << "]";
    logger = spdlog::stdout_logger_mt(oss.str());

    init();
}

WebsocketServer::WebsocketServer(boost::asio::io_service& ioService, uint16_t port) :
    ioService_(ioService)
{
    std::ostringstream oss;
    oss << "WebsocketServer-" << port;
    logger = spdlog::stdout_logger_mt(oss.str());

    init();
    startListening(port);
}

void WebsocketServer::startListening(uint16_t port)
{
    logger->info("startListening({})", port);
    try
    {
        server_.listen(port);
        server_.start_accept();
    }
    catch (const std::exception& e)
    {
        logger->error("startListening caught exception {}", e.what());
        throw;
    }
}

void WebsocketServer::sendToAll(const string& msg)
{
    logger->debug() << "WebsocketServer:sendToAll: " << msg;
    auto conns = connections();
    std::for_each(conns.begin(), conns.end(),
                  [this, &msg](const websocketpp::connection_hdl& handle)
        {
            server_.send(handle, msg, websocketpp::frame::opcode::text);
        });
}

void WebsocketServer::send(const string& remoteAddress, const string& msg)
{
    auto conns = connections();
    for (auto& conn: conns)
    {
        if (remoteAddress == remoteEndpoint(conn))
        {
            server_.send(conn, msg, websocketpp::frame::opcode::text);
            break;
        }
    }
}

void WebsocketServer::sendHttpResponse(ConnectionPtr conn, ResponsePtr response)
{
    auto& resp = *response;
    for (const auto& hdr : resp.headers)
        conn->append_header(hdr.first, hdr.second);

    conn->set_body(resp.content);
    conn->set_status(resp.httpStatus);

    conn->send_http_response();
}

void WebsocketServer::sendWebsocketResponse(ConnectionPtr conn, ResponsePtr response)
{
    conn->send(response->content, response->websocketOpCode);
}

std::vector<string> WebsocketServer::getConnectedClients()
{
    std::vector<string> clientAddresses;
    auto conns = connections();
    for (auto& connection: conns)
    {
        clientAddresses.push_back(remoteEndpoint(connection));
    }
    return clientAddresses;
}

std::size_t WebsocketServer::getConnectedClientCount() const
{
    return connections_.size();
}

void WebsocketServer::init()
{
    server_.set_http_handler(std::bind(&WebsocketServer::onHttp, this, std::placeholders::_1));
    server_.set_message_handler(std::bind(&WebsocketServer::onMessage, this, std::placeholders::_1, std::placeholders::_2));
    server_.set_open_handler(std::bind(&WebsocketServer::onSocketOpen, this, std::placeholders::_1));
    server_.set_close_handler(std::bind(&WebsocketServer::onSocketClose, this, std::placeholders::_1));

    server_.set_reuse_addr(true);
    server_.init_asio(&ioService_);
    // set the websocket server's log level
    server_.clear_access_channels(websocketpp::log::alevel::all);
    server_.set_access_channels(websocketpp::log::alevel::connect | websocketpp::log::alevel::disconnect |
                                /*websocketpp::log::alevel::http | */websocketpp::log::alevel::fail);
}

string WebsocketServer::remoteEndpoint(websocketpp::connection_hdl handle)
{
    Server::connection_ptr con = server_.get_con_from_hdl(handle);
    return remoteEndpoint(con);
}

string WebsocketServer::remoteEndpoint(Server::connection_ptr connection)
{
    return connection ? connection->get_remote_endpoint() : "";
}

void WebsocketServer::onSocketOpen(websocketpp::connection_hdl handle)
{
    logger->info("onSocketOpen:connection received from: {}", remoteEndpoint(handle));

    {
        std::unique_lock<std::mutex> lock(mutex_);
        connections_.insert(handle);
    }

    if (connectCb_)
    {
        Server::connection_ptr con = server_.get_con_from_hdl(handle);
        string remote = remoteEndpoint(con);
        const auto& request = con->get_request();
        const string& method = request.get_method();
        const string& uri = request.get_uri();
        connectCb_(con, remote, uri, method);
    }
}

void WebsocketServer::onSocketClose(websocketpp::connection_hdl handle)
{
    logger->info("onSocketClose:connection closed from: {}", remoteEndpoint(handle));
    if (disconnectCb_)
    {
        Server::connection_ptr con = server_.get_con_from_hdl(handle);
        string remote = remoteEndpoint(con);
        const auto& request = con->get_request();
        const string& method = request.get_method();
        const string& uri = request.get_uri();
        disconnectCb_(con, remote, uri, method);
    }

    {
        std::unique_lock<std::mutex> lock(mutex_);
        connections_.erase(handle);
    }
}

void WebsocketServer::onMessage(websocketpp::connection_hdl handle, Server::message_ptr msg)
{
    Server::connection_ptr con = server_.get_con_from_hdl(handle);
    if (wsMessageCb_)
    {
        wsMessageCb_(con, "", "", "", msg->get_payload());
    }
}

void WebsocketServer::onHttp(websocketpp::connection_hdl handle)
{
    Server::connection_ptr con = server_.get_con_from_hdl(handle);

    // Response will be sent later
    con->defer_http_response();

    string remote = remoteEndpoint(con);
    const string& requestMsg = con->get_request_body();
    //const string& header = con->get_request_header();
    const auto& request = con->get_request();
    const string& method = request.get_method();
    const string& uri = request.get_uri();

    logger->debug("http: uri:{0} method:{1} remote:{2} msg:{3}", uri, method, remote, requestMsg);
    if (httpMessageCb_)
    {
        httpMessageCb_(con, remote, uri, method, requestMsg);
    }
}

void WebsocketServer::setLogLevel(spdlog::level::level_enum level)
{
    logger->set_level(level);
}
