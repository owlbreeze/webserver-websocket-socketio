#ifndef websocket_client_base_INCLUDED
#define websocket_client_base_INCLUDED

#include <stdint.h>
#include <string>
#include <vector>
#include <functional>
#include <set>
#include <tuple>

#include <asio.hpp>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/client.hpp>
#include <spdlog/spdlog.h>


class WebsocketClientBase
{
public:

    WebsocketClientBase(asio::io_service& ioService, spdlog::level::level_enum logLevel, bool logToStdError = false) {}
    virtual ~WebsocketClientBase() {}

    virtual bool connect(const std::string& uri) = 0;
    virtual void send(const std::string& msg) = 0;
    virtual void close() = 0;

    virtual void setOpenCb(std::function<void(const std::string& serverId)> openCb) = 0;
    virtual void setFailCb(std::function<void(const std::string& serverId, const std::string& error)> failCb) = 0;
    virtual void setClosedCb(std::function<void(const std::string& serverId, const std::string& error)> closedCb) = 0;
    virtual void setMessageCb(std::function<void(const std::string&)> messageCb) = 0;

};

#endif // ifndef websocket_client_base_INCLUDED
