#include "socket_io.h"
#include <iostream>
#include <json.hpp>

using std::string;
using namespace nlohmann;

namespace {
    auto logger = spdlog::stdout_logger_mt("SocketIo");
}

namespace Parser
{
    namespace EngineIo
    {
        enum class Type
        {
            Open = '0',
            Close = '1',
            Ping = '2',
            Pong = '3',
            Message = '4',
            Upgrade = '5',
            Noop = '6',
            Undefined = 'z'
        };
    }

    namespace SocketIo
    {
        enum class Type
        {
            Connect = '0',
            Disconnect = '1',
            Event = '2',
            Ack = '3',
            Error = '4',
            BinaryEvent = '5',
            BinaryAck = '6',
            Undefined = 'z'
        };

        string createEventData(const std::string& eventType, const std::string& data)
        {
            return json({ eventType, data }).dump();
        }
    }

    string createEngineIoMsg(EngineIo::Type engineIoType, const string& data = string())
    {
        string ret;
        ret.reserve(data.size() + 10);
        ret.append(1, (static_cast<char>(engineIoType)))
           .append(data);
        logger->debug("createEngineIoMsg {0}", ret);
        return ret;
    }

    string createMsg(EngineIo::Type engineIoType, SocketIo::Type socketIoType, const string& data = string())
    {
        string ret;
        ret.reserve(data.size() + 10);
        ret.append(1, (static_cast<char>(engineIoType)))
           .append(1, (static_cast<char>(socketIoType)))
           .append(data);
        logger->debug("createMsg {0}", ret);
        return ret;
    }

    string createMsg(SocketIo::Type socketIoType, const string& data = string())
    {
        string ret;
        ret.reserve(data.size() + 10);
        ret.append(1, (static_cast<char>(EngineIo::Type::Message)))
           .append(1, (static_cast<char>(socketIoType)))
           .append(data);
        logger->debug("createMsg {0}", ret);
        return ret;
    }

    string createMsg(SocketIo::Type socketIoType, const std::string& nsp, const std::string& id, const string& data = string())
    {
        string ret;
        ret.reserve(data.size() + 10);
        ret.append(1, (static_cast<char>(EngineIo::Type::Message)));
        ret.append(1, (static_cast<char>(socketIoType)));
        if (nsp.size())
        {
            ret.push_back('/');
            ret.append(nsp);
            if (!id.empty() || !data.empty())
                ret.push_back(',');
        }
        ret.append(id);
        ret.append(data);
        logger->debug("createMsg  {0}", ret);
        return ret;
    }

    struct Msg
    {
        Msg() : engineIoType(EngineIo::Type::Undefined), type(SocketIo::Type::Undefined), nsp(1, '/') {}
        EngineIo::Type engineIoType;
        SocketIo::Type type;
        string nsp;
        string id;
        string data;
    };

    string msgToString(const Msg& msg)
    {
        std::ostringstream oss;
        oss << "eioType: " << static_cast<char>(msg.engineIoType) << " type:" << static_cast<char>(msg.type) << " nsp:" << msg.nsp << " id:" << msg.id << " data:" << msg.data;
        return oss.str();
    }

    enum class State
    {
        EngineIoType = 0,
        Type,
        PostField,
        Nsp,
        Id,
        Data
    };

    Msg parse(const std::string& data)
    {
        Msg msg;
        std::size_t len = data.size();
        if (len < 1)
            return msg;

        std::size_t ofs = 0;
        State state = State::EngineIoType;

        while (ofs < len)
        {
            auto d = data[ofs];

            switch (state)
            {
                case State::EngineIoType:
                    msg.engineIoType = static_cast<Parser::EngineIo::Type>(data[ofs]);
                    state = State::Type;
                    break;

                case State::Type:
                    msg.type = static_cast<SocketIo::Type>(data[ofs]);
                    state = State::PostField;
                    break;

                case State::PostField:
                    if (d == '/')
                    {
                        state = State::Nsp;
                        msg.nsp.clear();
                    }
                    else if (isdigit(d))
                    {
                        msg.id += d;
                        state = State::Id;
                    }
                    else
                    {
                        msg.data = data.substr(ofs);
                        ofs = len;
                    }
                    break;

                case State::Nsp:
                    if (d == ',')
                    {
                        state = State::PostField;
                    }
                    else
                    {
                        msg.nsp.push_back(d);
                    }
                    break;

                case State::Id:
                    if (isdigit(d))
                    {
                        msg.id += d;
                    }
                    else
                    {
                        // after Id is the payload, copy directly
                        msg.data = data.substr(ofs);
                        ofs = len;
                    }
                    break;

                default:
                    break;
            }

            ++ofs;
        }

        return msg;
    }
} // namespace Parser


SocketIo::SocketIo() :
    // need to provide the comparitor fn for shared_ptr
    connNamespaceMap_([](const WebServer::ConnectionPtr& lhs, const WebServer::ConnectionPtr& rhs) { return lhs.get() != rhs.get(); }),
    msgsHandled_(0)
{
}

void SocketIo::handleConnect(WebServer::ConnectionPtr conn, const string& remote, const string& uri, const string& method)
{
    connNamespaceMap_.emplace(conn, "");// "/");

    logger->debug("Adding connection {0}", connNamespaceMap_.size());
    json connectResponse = {
        { "sid", "6KqcHGq5MafxGWW0AAAA" },
        { "upgrades", json::array() },
        { "pingInterval", 5000 },
        { "pingTimeout", 10000 },
        { "perMessageDeflate", false },
        { "compress", false }
    };

    conn->send(Parser::createEngineIoMsg(Parser::EngineIo::Type::Open, connectResponse.dump()));
    conn->send(Parser::createMsg(Parser::SocketIo::Type::Connect, "", ""));
}

void SocketIo::handleDisconnect(WebServer::ConnectionPtr conn)
{
    auto it = connNamespaceMap_.find(conn);

    if (it != connNamespaceMap_.end())
    {
        logger->debug("Removing connection");
        connNamespaceMap_.erase(it);
        logger->debug("Removed connection, remaining:{0}", connNamespaceMap_.size());
    }
}

void SocketIo::handleMsg(WebServer::ConnectionPtr conn, const string& msg)
{
    logger->debug("handleMsg : {0}bytes {1}", msg.size(), msg);
#if 0
    auto it = std::find_if(connections_.begin(), connections_.end(), [&](const WebServer::ConnectionPtr& c)
        {
            return c.get() == conn.get();
        });

    if (it == connections_.end())
        return;
#endif
}

void SocketIo::handleMsg(WebServer::RequestPtr request)
{
    auto req = *request;
    logger->debug("handleMsg : {0}bytes {1}", req.msg.size(), req.msg);

    Parser::Msg msg = Parser::parse(req.msg);
    logger->debug("parsed:{0}", Parser::msgToString(msg));

    switch (msg.engineIoType)
    {
        case Parser::EngineIo::Type::Ping:
        {
            logger->debug("received ping");
            req.conn->send(Parser::createEngineIoMsg(Parser::EngineIo::Type::Pong));
            break;
        }
        case Parser::EngineIo::Type::Message:
        {
            switch (msg.type)
            {
                case Parser::SocketIo::Type::Connect:
                {
                    req.conn->send(Parser::createMsg(Parser::SocketIo::Type::Connect, msg.nsp, ""));
                    break;
                }

                case Parser::SocketIo::Type::Event:
                {
                    processEvent(req.conn, msg.data, msg.nsp, msg.id);
                    break;
                }

                default:
                    break;
            }
            break;
        }

        default:
            break;
    }

    ++msgsHandled_;
}

void SocketIo::emitEvent(const string& eventType, const string& data)
{
    logger->debug("emitEvent : {0} : {1}", eventType, data);
    string messageData = Parser::SocketIo::createEventData(eventType, data);
    for (auto& connNamespacePair : connNamespaceMap_)
    {
        emitEvent(connNamespacePair.first, messageData);
    }
}

void SocketIo::emitEvent(WebServer::ConnectionPtr conn, const string& eventType, const string& data)
{
    logger->debug("emitEvent : {0} : {1}", eventType, data);
    emitEvent(conn, Parser::SocketIo::createEventData(eventType, data));
}

void SocketIo::emitEvent(WebServer::ConnectionPtr conn, const string& eventDataString)
{
    logger->debug("emitEvent : {0}", eventDataString);
    auto it = connNamespaceMap_.find(conn);

    conn->send(Parser::createMsg(Parser::SocketIo::Type::Event,
                                 it == connNamespaceMap_.end() ? "" : (*it).second,
                                 "",
                                 eventDataString));
}

void SocketIo::emitEvent(WebServer::ConnectionPtr conn, const std::string& eventType, const nlohmann::json& data)
{
    logger->debug("emitEvent : {0} {1}", eventType, data.dump());
    auto it = connNamespaceMap_.find(conn);

    string eventDataString = json({eventType, data}).dump();
    conn->send(Parser::createMsg(Parser::SocketIo::Type::Event,
                                 it == connNamespaceMap_.end() ? "" : (*it).second,
                                 "",
                                 eventDataString));

}

void SocketIo::on(const string& eventType, EventCb eventHandler)
{
    handlers_[eventType] = eventHandler;
}

void SocketIo::completeCb(bool success, WebServer::ConnectionPtr conn, const std::string& nsp, const std::string& id)
{
    logger->debug("completeCb : {0} : {1}", (success ? "true" : "false"), id);
    if (id == "50000")
        logger->info("completeCb : {0} : {1}", (success ? "true" : "false"), id);
    conn->send(Parser::createMsg(Parser::SocketIo::Type::Ack, nsp, id, json({success}).dump()));
}

bool SocketIo::processEvent(WebServer::ConnectionPtr conn, const std::string& event, const std::string& nsp, const std::string& id)
{
    try
    {
        json eventJson = json::parse(event);

        if (!eventJson.is_array() || eventJson.empty())
        {
            logger->debug("processEvent : event json invalid");
            return false;
        }

        const string& eventType = eventJson[0];

        auto it = handlers_.find(eventType);
        if (it != handlers_.end())
        {
            const EventCb& eventCb = (*it).second;
            return eventCb(conn, eventJson.size() > 1 ? eventJson[1] : "",
                           std::bind(&SocketIo::completeCb, this, std::placeholders::_1, conn, nsp, id));
        }
        logger->debug("no handler assigned to event '{0}'", eventType);
    }
    catch (const std::exception& e)
    {
        logger->info("processEvent : Caught exception while parsing json: {0}", e.what());
    }

    return false;
}

std::size_t SocketIo::msgsHandled() const
{
    return msgsHandled_;
}
