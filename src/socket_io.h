
#ifndef socket_io_INCLUDED
#define socket_io_INCLUDED

#include <string>
#include <functional>
#include <map>
#include <json.hpp>
#include "comms/webserver.h"

/// Closely tied to the Webserver implementation
class SocketIo
{
public:

    typedef std::function<void(bool ack)> CompleteCb;
    typedef std::function<bool(WebServer::ConnectionPtr conn, const nlohmann::json& data, CompleteCb completeCb)> EventCb;

    SocketIo();

    void handleConnect(WebServer::ConnectionPtr conn, const std::string& remote, const std::string& uri, const std::string& method);
    void handleDisconnect(WebServer::ConnectionPtr conn);

    void handleMsg(WebServer::ConnectionPtr conn, const std::string& msg);
    void handleMsg(WebServer::RequestPtr request);

    /// Emit to all clients
    void emitEvent(const std::string& eventType, const std::string& data);
    void emitEvent(WebServer::ConnectionPtr conn, const std::string& eventType, const std::string& data);
    void emitEvent(WebServer::ConnectionPtr conn, const std::string& eventType, const nlohmann::json& data);

    void on(const std::string& eventType, EventCb eventHandler);

    std::size_t msgsHandled() const;
private:

    void emitEvent(WebServer::ConnectionPtr conn, const std::string& eventDataString);
    bool processEvent(WebServer::ConnectionPtr conn, const std::string& event, const std::string& nsp = "", const std::string& id = "");
    void completeCb(bool success, WebServer::ConnectionPtr conn, const std::string& nsp, const std::string& id);

    std::map<WebServer::ConnectionPtr, std::string, bool(*)(const WebServer::ConnectionPtr&, const WebServer::ConnectionPtr&)> connNamespaceMap_;
    std::map<std::string, EventCb> handlers_;
    std::size_t msgsHandled_;
};

#endif // ifndef socket_io_INCLUDED
