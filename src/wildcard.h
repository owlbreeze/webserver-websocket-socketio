#ifndef wildcard_INCLUDED
#define wildcard_INCLUDED

#include <iostream>


inline bool wildcard_compare(const char *wildcardString, const char *testString)
{
    // std::cout << "comparing: " << wildcardString << " and " << testString << std::endl;
    const char* cp = NULL;
    const char* mp = NULL;

    while ((*testString) && (*wildcardString != '*'))
    {
        if ((*wildcardString != *testString) && (*wildcardString != '?'))
        {
            return false;
        }
        wildcardString++;
        testString++;
    }

    while (*testString)
    {
        if (*wildcardString == '*')
        {
            if (!*++wildcardString)
            {
                return 1;
            }
            mp = wildcardString;
            cp = testString+1;
        }
        else if ((*wildcardString == *testString) || (*wildcardString == '?'))
        {
            wildcardString++;
            testString++;
        }
        else
        {
            wildcardString = mp;
            testString = cp++;
        }
    }

    while (*wildcardString == '*')
    {
        wildcardString++;
    }

    return (*wildcardString) == 0 ? true : false;
}


#endif // ifndef wildcard_INCLUDED
