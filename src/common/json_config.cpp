#include "json_config.h"
#include <boost/filesystem.hpp>
#include <memory>
#include <iostream>
#include <fstream>
#include <list>


using std::string;

namespace
{
    nlohmann::json mergeJson(const nlohmann::json& oldJson, const nlohmann::json& newJson)
    {
        nlohmann::json j = oldJson;
        if (newJson.is_array())
        {
            /// \todo Can't really handle this.
        }
        else if (newJson.is_object())
        {
            for (nlohmann::json::const_iterator it = newJson.begin(); it != newJson.end(); ++it)
            {
                j[it.key()] = it.value();
            }
        }
        return j;
    }

    nlohmann::json processFile(const boost::filesystem::path& path)
    {
        std::ifstream configFile(path.string().c_str());
        try
        {
            return nlohmann::json::parse(configFile);
        }
        catch (const std::exception& e)
        {
            return nlohmann::json();
        }

    }

    nlohmann::json processDir(const boost::filesystem::path& path)//, nlohmann::json& jsonCfg)
    {
        nlohmann::json retJson({});

        // sort
        std::list<boost::filesystem::path> paths;
        std::copy(boost::filesystem::directory_iterator(path), boost::filesystem::directory_iterator(), std::back_inserter(paths));
        paths.sort();

        for (const boost::filesystem::path& p : paths)
        {
            try
            {
                if (!boost::filesystem::exists(p))
                    continue;

                if (boost::filesystem::is_directory(p))
                {
                    if (p.string() != "." && p.string() != "..")
                    {
                        nlohmann::json j = processDir(p);//, jsonCfg);
                        retJson = mergeJson(retJson, j);
                    }
                }
                else if (boost::filesystem::is_regular_file(p) || boost::filesystem::is_symlink(p))
                {
                    nlohmann::json j = processFile(p);
                    retJson = mergeJson(retJson, j);
                }
            }
            catch (const boost::filesystem::filesystem_error& e)
            {
            }
        }
        return retJson;
    }

} // namespace

JsonConfig::JsonConfig() : json_({})
{
}

void JsonConfig::setPaths(const std::vector<std::string>& paths)
{
    configPaths_ = paths;
    update();
}

JsonConfig& JsonConfig::instance()
{
    static std::unique_ptr<JsonConfig> configInstance;

    if (!configInstance)
        configInstance.reset(new JsonConfig());

    return (*configInstance);
}

JsonConfig& cfg()
{
    return JsonConfig::instance();
}

void JsonConfig::update()
{
    for (const auto& cfgPath : configPaths_)
    {
        try
        {
            boost::filesystem::path p(cfgPath);

            if (!boost::filesystem::exists(p))
                continue;

            if (boost::filesystem::is_directory(p))
            {
                nlohmann::json j = processDir(p);
                json_ = mergeJson(json_, j);
            }
            else if (boost::filesystem::is_regular_file(p) ||
                     boost::filesystem::is_symlink(p))
            {
                nlohmann::json j = processFile(p);
                json_ = mergeJson(json_, j);
            }
        }
        catch (const boost::filesystem::filesystem_error& e)
        {
        }
    }
}
