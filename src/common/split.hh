#ifndef split_INCLUDED
#define split_INCLUDED

#include <string>
#include <vector>
#include <sstream>

namespace Utils
{

inline std::vector<std::string> split(const std::string &s, char delim)
{
    std::vector<std::string> elems;

    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim))
    {
        elems.push_back(item);
    }
    return elems;
}


} // namespace Utils

#endif // ifndef split_INCLUDED
