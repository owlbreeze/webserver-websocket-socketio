#ifndef url_INCLUDED
#define url_INCLUDED

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <map>
#include "split.hh"


struct Url
{
    Url() {}

    Url(const std::string& url) : url_(url)
    {
        parse(url);
    }

    Url& parse(const std::string& url)
    {
        static const std::string protocolEnd("://");
        std::string::const_iterator protocolIt = std::search(url.begin(), url.end(), protocolEnd.begin(), protocolEnd.end());
        protocol_.reserve(distance(url.begin(), protocolIt));
        std::transform(url.begin(), protocolIt, std::back_inserter(protocol_), std::ptr_fun<int,int>(tolower)); // protocol is icase
        if (protocolIt == url.end())
            return *this;

        std::advance(protocolIt, protocolEnd.length());
        std::string::const_iterator pathIt = std::find(protocolIt, url.end(), '/');
        host_.reserve(std::distance(protocolIt, pathIt));
        std::transform(protocolIt, pathIt, std::back_inserter(host_), std::ptr_fun<int,int>(tolower)); // host is icase

        host_ = decode(host_);  //  urldecode

        return parsePathAndQuery(std::string(pathIt, url.end()));
    }

    // extract query components
    Url& parsePathAndQuery(const std::string& pathAndQuery)
    {
        std::string::const_iterator queryIt = std::find(pathAndQuery.begin(), pathAndQuery.end(), '?');
        path_.assign(pathAndQuery.begin(), queryIt);

        path_ = decode(path_);  //  urldecode

        if (queryIt != pathAndQuery.end())
            ++queryIt;

        query_.assign(queryIt, pathAndQuery.end());

        std::vector<std::string> queries = Utils::split(query_, '&');
        for (const auto& queryParam : queries)
        {
            std::vector<std::string> queries = Utils::split(queryParam, '=');
            if (queries.size() > 0)
                queryParams_[decode(queries[0])] = queries.size() > 1 ? decode(queries[1]) : "";
        }

        return *this;
    }

    static std::string encode(const std::string& value)
    {
        std::ostringstream escaped;
        escaped.fill('0');
        escaped << std::hex;

        for (std::string::const_iterator i = value.begin(), n = value.end(); i != n; ++i)
        {
            std::string::value_type c = (*i);

            // Keep alphanumeric and other accepted characters intact
            if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~')
            {
                escaped << c;
                continue;
            }

            // Any other characters are percent-encoded
            escaped << std::uppercase;
            escaped << '%' << std::setw(2) << int((unsigned char) c);
            escaped << std::nouppercase;
        }
        return escaped.str();
    }

    static std::string decode(const std::string& src)
    {
        std::string ret;
        char ch;
        int val;
        for (size_t i = 0; i < src.size(); ++i)
        {
            if (src[i] == '%')
            {
                sscanf(src.substr(i+1, 2).c_str(), "%x", &val);
                ch = static_cast<char>(val);
                ret += ch;
                i = i + 2;
            }
            else
            {
                ret.push_back(src[i]);
            }
        }
        return ret;
    }

    const std::string& url() const { return url_; }
    const std::string& protocol() const { return protocol_; }
    const std::string& host() const { return host_; }
    const std::string& path() const { return path_; }
    const std::string& query() const { return query_; }
    const std::map<std::string, std::string>& queryParams() const { return queryParams_; }

private:
    std::string url_;

    std::string protocol_;
    std::string host_;
    std::string path_;
    std::string query_;
    std::map<std::string, std::string> queryParams_;
};

#endif // ifndef url_INCLUDED
