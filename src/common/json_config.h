#ifndef json_config_INCLUDED
#define json_config_INCLUDED

#include <string>
#include <vector>
#include <stdexcept>
#include <json.hpp>

class JsonConfig
{
public:

    static JsonConfig& instance();

    /// Set the path (file or dir) to process all files within.
    void setPaths(const std::vector<std::string>& paths);

    template<typename T>
    inline T get(const std::vector<std::string>& keyChain, const T& defaultValue = T())
    {
        return jsonGetValue(keyChain, json_, defaultValue);
    }

    const nlohmann::json& json() const { return json_; }

protected:
    JsonConfig();

private:
    void update();

    template<typename T>
    inline T jsonGetValue(const std::vector<std::string>& keyChain, const nlohmann::json& cfg, const T& defaultValue = T())
    {
        try
        {
            auto it = keyChain.begin();
            if (cfg.count(*it))
            {
                if (keyChain.begin() + 1 == keyChain.end())
                {
                    T value = cfg[*it];
                    return value;
                }
                else
                {
                    nlohmann::json obj = cfg[*it];
                    return jsonGetValue<T>(std::vector<std::string>(keyChain.begin() + 1, keyChain.end()), obj, defaultValue);
                }
            }
        }
        catch (const std::exception& e)
        {
        }
        return defaultValue;
    }


    std::vector<std::string> configPaths_;
    nlohmann::json json_;
};

/// Get the instance.
JsonConfig& cfg();

#endif // json_config_INCLUDED
