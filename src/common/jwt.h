#ifndef jwt_INCLUDED
#define jwt_INCLUDED

#include <stdint.h>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <iomanip>

#include <openssl/x509.h>
#include <openssl/hmac.h>

#include <json.hpp>
#include "base64.hpp"

namespace Jwt
{
    enum class Algorithm
    {
        None = 0,
        HS256,
        HS384,
        HS512,
        RS256,
        RS384,
        RS512,
        Unknown
    };

    const char* algorithmToStr(Algorithm alg)
    {
        static const std::map<Algorithm, const char*> algMap = {
            { Algorithm::None, "none" },
            { Algorithm::HS256, "HS256" },
            { Algorithm::HS384, "HS384" },
            { Algorithm::HS512, "HS512" },
            { Algorithm::RS256, "RS256" },
            { Algorithm::RS384, "RS384" },
            { Algorithm::RS512, "RS512" }
        };

        const auto it = algMap.find(alg);
        return it == algMap.end() ? nullptr : (*it).second;
    }

    Algorithm algorithmToStr(const std::string& algStr)
    {
        static const std::map<std::string, Algorithm> algMap = {
            { "none", Algorithm::None },
            { "HS256", Algorithm::HS256 },
            { "HS384", Algorithm::HS384 },
            { "HS512", Algorithm::HS512 },
            { "RS256", Algorithm::RS256 },
            { "RS384", Algorithm::RS384 },
            { "RS512", Algorithm::RS512 }
        };

        const auto it = algMap.find(algStr);
        return it == algMap.end() ? Algorithm::Unknown : (*it).second;
    }

    std::string signHS256(const std::string& dataToSign, const std::string& key)
    {
        unsigned int len = 32;
        unsigned char signedData[32];

        HMAC_CTX ctx;
        HMAC_CTX_init(&ctx);

        HMAC_Init_ex(&ctx, (const unsigned char*)key.data(), key.size(), EVP_sha256(), NULL);
        HMAC_Update(&ctx, (const unsigned char*)dataToSign.data(), dataToSign.size());
        HMAC_Final(&ctx, signedData, &len);
        HMAC_CTX_cleanup(&ctx);

        std::ostringstream oss;
        oss << std::setw(2) << std::setfill('0');

        for (unsigned int i = 0; i < len; i++)
        {
            oss << std::hex << static_cast<int>(signedData[i]);
        }

        return oss.str();
    }

    std::string buildJwt(Algorithm alg, nlohmann::json payload, const std::string& secret)
    {
        //const nlohmann::json header({ { "typ","jwt" }, { "alg", algorithmToStr(alg) } });
        const std::string header = std::string("{ \"typ\": \"jwt\", \"alg\": \"") + algorithmToStr(alg) + std::string("\" }");
        std::string jwtHdrPayload = base64::urlEncode(base64::encode(header/*.dump()*/)) + "." +
                                    base64::urlEncode(base64::encode(payload.dump()));

        std::string hash;
        switch (alg)
        {
            case Algorithm::HS256:
                hash = signHS256(jwtHdrPayload, secret);
                break;
            default:
                break;
        }

        return jwtHdrPayload + "." + base64::urlEncode(base64::encode(hash));
    }

    std::vector<std::string> split(const std::string& s, char delim)
    {
        std::vector<std::string> elems;
        std::istringstream ss(s);
        std::string item;
        while (std::getline(ss, item, delim))
        {
            elems.push_back(item);
        }
        return elems;
    }

    nlohmann::json decodeJwt(const std::string& jwt, const std::string& secret, bool permitNone = false)
    {
        auto elems = split(jwt, '.');
        if (elems.size() != 3)
            return nlohmann::json(nullptr);

        const std::string& headerBase64 = elems[0];
        const std::string& payloadBase64 = elems[1];
        const std::string& signatureBase64 = elems[2];

        try
        {
            const std::string headerStr = base64::decode(base64::urlDecode(headerBase64));
            const nlohmann::json header = nlohmann::json::parse(headerStr);

            if ((!header.count("typ") || header["typ"].get<std::string>() != "jwt") ||
                !header.count("alg"))
            {
                return nlohmann::json(nullptr);
            }

            Algorithm alg = algorithmToStr(header["alg"].get<std::string>());
            switch (alg)
            {
                case Algorithm::HS256:
                {
                    std::string hash = signHS256(headerBase64 + "." + payloadBase64, secret);
                    std::string hashBase64UrlEnc = base64::urlEncode(base64::encode(hash));
                    if (signatureBase64 != hashBase64UrlEnc)
                    {
                        // signature failed
                        return nlohmann::json(nullptr);
                    }
                    break;
                }
                case Algorithm::None:
                {
                    if (!permitNone)
                        return nlohmann::json(nullptr);
                    break;
                }
                default:
                    // unsupported alg
                    return nlohmann::json(nullptr);
                    break;
            }

            const std::string payloadStr = base64::decode(base64::urlDecode(payloadBase64));
            const nlohmann::json payload = nlohmann::json::parse(payloadStr);
            return payload;
        }
        catch (const std::exception& e)
        {
            return nlohmann::json(nullptr);
        }

        // decode header, verify it's 'typ' is 'jwt', then get 'alg'
        // use 'alg' to re-verify the hash
        // decode payload

        return nlohmann::json(nullptr);
    }

#if 0
    std::string signRSA(const std::string &data, RSA *r)
    {
        uint8_t  digest[SHA512_DIGEST_LENGTH];
        uint32_t digest_len;

        uint32_t type;

        if (!r)
            throw std::invalid_argument("Invalid RSA object");

        switch (m_alg)
        {
        case JWT_ALG_RS256: {
            SHA256_CTX sha_ctx;

            digest_len = SHA256_DIGEST_LENGTH;
            type = NID_sha256;

            if (SHA256_Init(&sha_ctx) != 1) {

            }

            if (SHA256_Update(&sha_ctx, (const uint8_t *)data.c_str(), data.size()) != 1) {
                throw std::runtime_error("Couldn't calculate hash");
            }

            if (SHA256_Final(digest, &sha_ctx) != 1) {
                throw std::runtime_error("Couldn't finalize SHA");
            }

            break;
        }
        default:
            break;
        }

        uint32_t sig_len;

        std::vector<uint8_t> sig(RSA_size(r));
        //uint8_t *sig = new uint8_t[RSA_size(r)];

        if (RSA_sign(type, digest, digest_len, sig.data(), sig.size(), r) != 1) {
            //delete[] sig;
            throw std::runtime_error("RSA sign failed");
        }

        //signature.insert(0, (char *)sig, sig_len);
        tools::base64::encode(signature, sig, sig_len);

        base64uri_encode(signature);

        base64uri_encode();
        //delete[] sig;
    }

#endif
} // namespace Jwt

#if 0

class Jwt
{
    enum class Algorithm
    {
        JWT_ALG_NONE = 0,
        JWT_ALG_HS256,
        JWT_ALG_HS384,
        JWT_ALG_HS512,
        JWT_ALG_RS256,
        JWT_ALG_RS384,
        JWT_ALG_RS512,
        JWT_ALG_UKNOWN
    };

public:
    Jwt(Algorithm alg) : alg_(alg)
    {

    }

    /// Verify the token
    Jwt(const std::string& token);

    void grantAdd(const std::string &key, const std::string &value);
    bool grantVerify(const std::string &key, const std::string &value);

    void sign(std::string &token, const uint8_t *key, size_t key_size);
    void sign(std::string &token, RSA *r);

    bool verify(const uint8_t *key, size_t size);
    bool verify(RSA *r);

};
#endif


#endif // ifndef jwt_INCLUDED
