/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef AVRO_TEST_HH_2293902145__H_
#define AVRO_TEST_HH_2293902145__H_


#include <sstream>
#include "boost/any.hpp"
#include "avro/Specific.hh"
#include "avro/Encoder.hh"
#include "avro/Decoder.hh"

struct lvl3_record {
    std::string item1_lvl3;
    std::string item2_lvl3;
    lvl3_record() :
        item1_lvl3(std::string()),
        item2_lvl3(std::string())
        { }
};

struct lvl2_record {
    std::string item1_lvl2;
    std::vector<lvl3_record > item2_lvl2;
    lvl2_record() :
        item1_lvl2(std::string()),
        item2_lvl2(std::vector<lvl3_record >())
        { }
};

struct myrecord {
    int32_t uid;
    std::string somefield;
    std::vector<lvl2_record > options;
    myrecord() :
        uid(int32_t()),
        somefield(std::string()),
        options(std::vector<lvl2_record >())
        { }
};

namespace avro {
template<> struct codec_traits<lvl3_record> {
    static void encode(Encoder& e, const lvl3_record& v) {
        avro::encode(e, v.item1_lvl3);
        avro::encode(e, v.item2_lvl3);
    }
    static void decode(Decoder& d, lvl3_record& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.item1_lvl3);
                    break;
                case 1:
                    avro::decode(d, v.item2_lvl3);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.item1_lvl3);
            avro::decode(d, v.item2_lvl3);
        }
    }
};

template<> struct codec_traits<lvl2_record> {
    static void encode(Encoder& e, const lvl2_record& v) {
        avro::encode(e, v.item1_lvl2);
        avro::encode(e, v.item2_lvl2);
    }
    static void decode(Decoder& d, lvl2_record& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.item1_lvl2);
                    break;
                case 1:
                    avro::decode(d, v.item2_lvl2);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.item1_lvl2);
            avro::decode(d, v.item2_lvl2);
        }
    }
};

template<> struct codec_traits<myrecord> {
    static void encode(Encoder& e, const myrecord& v) {
        avro::encode(e, v.uid);
        avro::encode(e, v.somefield);
        avro::encode(e, v.options);
    }
    static void decode(Decoder& d, myrecord& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.uid);
                    break;
                case 1:
                    avro::decode(d, v.somefield);
                    break;
                case 2:
                    avro::decode(d, v.options);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.uid);
            avro::decode(d, v.somefield);
            avro::decode(d, v.options);
        }
    }
};

}
#endif
