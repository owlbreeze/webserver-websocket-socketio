
#ifndef session_INCLUDED
#define session_INCLUDED

#include <stdint.h>
#include <string>
#include <memory>
#include <mutex>
#include <json.hpp>
#include <boost/optional.hpp>

namespace Session
{

class Session
{
public:
    Session() {}
    Session(const std::string& id, const nlohmann::json& data) : id_(id), data_(data) {}

    const std::string& id() const { return id_; }
    const nlohmann::json& data() const { return data_; }

private:

    std::string id_;
    nlohmann::json data_;
};


class Manager
{
public:
    virtual ~Manager() {}

    virtual boost::optional<const Session&> getSession(const std::string& id) const = 0;
    virtual boost::optional<Session&> getSession(const std::string& id) = 0;

    virtual Session& createSession(const std::string& id, const nlohmann::json& sessionData) = 0;
    virtual bool deleteSession(const std::string& id) = 0;

    virtual size_t activeSessions() const = 0;

};

class ManagerHashMap: public Manager
{
public:
    ManagerHashMap() {}

    virtual boost::optional<const Session&> getSession(const std::string& id) const override
    {
        std::unique_lock<std::mutex> lock(mutex_);
        const auto it = sessions_.find(id);
        return it == sessions_.end() ? boost::optional<const Session&>() : boost::optional<const Session&>((*it).second);
    }

    virtual boost::optional<Session&> getSession(const std::string& id) override
    {
        std::unique_lock<std::mutex> lock(mutex_);
        const auto it = sessions_.find(id);
        return it == sessions_.end() ? boost::optional<Session&>() : boost::optional<Session&>((*it).second);
    }

    virtual Session& createSession(const std::string& id, const nlohmann::json& sessionData) override
    {
        std::unique_lock<std::mutex> lock(mutex_);

        const auto it = sessions_.find(id);
        if (it != sessions_.end())
        {
            throw std::runtime_error("Session already exists");
        }
        sessions_.emplace(id, Session(id, sessionData));
        return sessions_[id];
    }

    virtual bool deleteSession(const std::string& id) override
    {
        std::unique_lock<std::mutex> lock(mutex_);

        bool deleted = false;
        const auto it = sessions_.find(id);
        if (it != sessions_.end())
        {
            sessions_.erase(it);
            deleted = true;
        }
        return deleted;
    }

    virtual size_t activeSessions() const override
    {
        std::unique_lock<std::mutex> lock(mutex_);
        return sessions_.size();
    }

private:

    mutable std::mutex mutex_;
    std::map<std::string, Session> sessions_;
};


//class ManagerDb: public Manager


} // namespace Session

template<typename ManagerType = Session::ManagerHashMap>
inline Session::Manager& session()
{
    static std::unique_ptr<Session::Manager> manager = nullptr;
    if (!manager)
    {
        manager.reset(new ManagerType());
    }

    return *manager;
}

#endif // ifndef session_INCLUDED
