
#ifndef TQueue_INCLUDED
#define TQueue_INCLUDED

#include <stdint.h>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <boost/optional.hpp>

//------------------------------------------------------------------------------

/// Queue implementation.
/// Thread safe.
template<typename T>
class TQueue
{
public:

    TQueue(std::size_t maxQueueSize = 0) : maxQueueSize_(maxQueueSize) {}

    /// Push an object onto the queue.
    bool push(const T& item)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if ((maxQueueSize_ <= 0) || (queue_.size() < maxQueueSize_))
        {
            queue_.push(item);
            cond_.notify_one();
            return true;
        }
        return false;
    }

    /// Pop an object from the queue.
    boost::optional<T> pop()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (!queue_.empty())
        {
            boost::optional<T> item = queue_.front();
            queue_.pop();
            return item;
        }
        return boost::none;
    }

    /// Wait until an object is present in the queue.
    void wait()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        cond_.wait(lock, [this]() { return !queue_.empty(); });
        cond_.notify_one();
    }

    /// Wait for an object in the queue for a maximum amount of time.
    /// Returns the number of objects in the Queue.
    std::size_t waitTimed(uint32_t timeoutMs)
    {
        auto now = std::chrono::system_clock::now();
        //std::lock_guard<std::mutex> lock(mutex_);
        std::unique_lock<std::mutex> lock(mutex_);
        cond_.wait_until(lock, now + std::chrono::milliseconds(timeoutMs),[this]() { return !queue_.empty(); });
        cond_.notify_one();
        return queue_.size();
    }

    /// Get the number of items in the queue.
    std::size_t queueSize() const
    {
        std::lock_guard<std::mutex> lock(mutex_);
        return queue_.size();
    }

private:
  
    std::queue<T> queue_;
    std::size_t maxQueueSize_;

    mutable std::mutex mutex_;
    std::condition_variable cond_;

}; // class Queue

#endif // ifndef ThreadsafeQueue_INCLUDED
