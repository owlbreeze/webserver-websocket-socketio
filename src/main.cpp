#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <functional>
#include <unordered_set>
#include <set>
#include <thread>
#include <mutex>

#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>

#include <json.hpp>
#include <spdlog/spdlog.h>
#include <base64.hpp>

#include "comms/websocket_server.h"
#include "comms/webserver.h"
#include "common/json_config.h"
#include "common/jwt.h"
#include "common/url.hh"
#include "socket_io.h"
#include "session.h"
#include "tqueue.h"

#define AVRO_TESTING
#ifdef AVRO_TESTING
#include "avro/Compiler.hh"
#include "avro/Encoder.hh"
#include "avro/Decoder.hh"
#include "avro_test.hh"
#endif

#define CURL_ASIO_TESTING
#ifdef CURL_ASIO_TESTING
#include <boost/smart_ptr.hpp>
#include "curl-asio.h"
#endif

using std::cout;
using std::cerr;
using std::endl;
using std::dec;
using std::hex;
using std::string;
using std::vector;
using std::map;
using std::pair;
using namespace nlohmann;

static const int webWorkerThreadsDefault = 1;

spdlog::level::level_enum logLevelFromString(const string& levelStr)
{
    static std::map<string, spdlog::level::level_enum> levelMap = {
        { spdlog::level::to_str(spdlog::level::trace), spdlog::level::trace },
        { spdlog::level::to_str(spdlog::level::debug), spdlog::level::debug },
        { spdlog::level::to_str(spdlog::level::info), spdlog::level::info },
        { spdlog::level::to_str(spdlog::level::notice), spdlog::level::notice },
        { spdlog::level::to_str(spdlog::level::warn), spdlog::level::warn },
        { spdlog::level::to_str(spdlog::level::err), spdlog::level::err },
        { spdlog::level::to_str(spdlog::level::critical), spdlog::level::critical },
        { spdlog::level::to_str(spdlog::level::alert), spdlog::level::alert },
        { spdlog::level::to_str(spdlog::level::emerg), spdlog::level::emerg },
        { spdlog::level::to_str(spdlog::level::off), spdlog::level::off }
    };

    auto it = levelMap.find(levelStr);
    return (it == levelMap.end() ? spdlog::level::info : (*it).second);
}

inline string intToString(int val)
{
    char valStr[32];
    snprintf(valStr, 31, "%d", val);
    return string(valStr);
}

void printUsage(const string& programName);

int main(int argc, char* const argv[])
{
    auto logger = spdlog::stdout_logger_mt("log");
    spdlog::set_level(spdlog::level::info);
    spdlog::set_pattern("[%n] [%L] [%Y-%m-%dT%H:%M:%S.%F%z] [%t] %v");

    logger->info() << "Starting: " << string(argv[0]);

    vector<string> configPaths;
    std::vector<uint16_t> webserverPorts;
    string logLevel;
    int webWorkerThreadCount = -1;
    int exitAfterMsgCount = -1;
    string targetUrl;
    bool curlCache = true;

    int c;
    while ((c = getopt (argc, argv, "?c:p:l:w:x:t:r")) != -1)
    {
        switch (c)
        {
            case 'c':
            {
                configPaths.push_back(optarg);
                break;
            }
            case 'p':
            {
                webserverPorts.push_back(atoi(optarg));
                break;
            }
            case 'l':
            {
                logLevel = string(optarg);
                break;
            }
            case 'w':
            {
                webWorkerThreadCount = atoi(optarg);
                break;
            }
            case 'x':
            {
                exitAfterMsgCount = atoi(optarg);
                break;
            }
            case 'r':
            {
                curlCache = false;
                break;
            }
            case 't':
            {
                targetUrl = string(optarg);
                break;
            }
            default:
            {
                char tempchar = (char)c;
                logger->error() << "Unknown option " << string(&tempchar, 1);
                printUsage(string(argv[0]));
                exit(1);
                break;
            }
        }
    }

    cfg().setPaths(configPaths);

    if (logLevel.empty())
    {
        logLevel = cfg().get<string>({"log", "level"}, "info");
    }
    spdlog::set_level(logLevelFromString(logLevel));

    if (webserverPorts.empty())
    {
        webserverPorts = cfg().get<std::vector<uint16_t>>({"webserver", "ports"}, { 80 });
    }

    if (webWorkerThreadCount <= 0)
    {
        webWorkerThreadCount = cfg().get<int>({"webserver", "workerThreads"}, webWorkerThreadsDefault);
    }

    if (targetUrl.empty())
    {
        targetUrl = cfg().get<string>({"targetUrl"}, "http://localhost");
    }

    logger->info() << "Active Sessions: " << session().activeSessions();

    boost::asio::io_service ioService;

    WebServer::WebsocketMessageFn webserverMsgCb = [&logger](WebServer::RequestPtr request, WebServer::ResponsePtr response)
        {
//            const WebServer::Request& req = *request;
            auto req = *request;
            logger->info("webserverMsgCb: route:{0}, remote:{1}, uri:{2}, method:{3}, msg:{4}", req.route, req.remote, req.uri, req.method, req.msg);
            return false;
        };

    WebServer::HttpReqFn httpReqCb = [&](WebServer::RequestPtr request, WebServer::ResponsePtr response)
        {
//            const WebServer::Request& req = *request;
            auto req = *request;
            logger->info("webserverHttpCb : route:{0}, remote:{1}, uri:{2}, method:{3}, msg:{4}", req.route, req.remote, req.uri, req.method, req.msg);
            return false;
        };

    SocketIo socketIo;//("/socket.io/");

    socketIo.on("msgPing", [&](WebServer::ConnectionPtr conn, const json& data, SocketIo::CompleteCb completeCb)
        {
            logger->debug("socketIo.on 'msgPing'");
            socketIo.emitEvent(conn, "msgPong", json({"pong", data}));
            if (completeCb)
                completeCb(true);
            return true;
        });

#ifdef CURL_ASIO_TESTING
    curl::multi curlMulti(ioService);

//    static uint64_t curlInstanceId = 0;
//    struct CurlInstancePtr
//    {
//        bool active;
//        std::shared_ptr<curl::easy> easy;
//    };
//    map<uint64_t, CurlInstancePtr> curlInstances;

    std::mutex curlEasyCacheMutex;
    map<string, vector<std::shared_ptr<curl::easy>>> curlEasyCacheMap;
    auto getCurlEasy = [curlCache, &curlMulti, &curlEasyCacheMutex, &curlEasyCacheMap](const string& url, const std::shared_ptr<std::ostream>& sink) //boost::shared_ptr<std::ostream> sink)
        {
            std::lock_guard<std::mutex> lock(curlEasyCacheMutex);

            auto it = curlEasyCacheMap.find(url);
            if (it == curlEasyCacheMap.end())
            {
                auto easy = std::make_shared<curl::easy>(curlMulti);
                easy->set_url(url);
                easy->set_sink(sink);
                return easy;
            }

            vector<std::shared_ptr<curl::easy>>& easys = (*it).second;
            if (easys.empty())
            {
                auto easy = std::make_shared<curl::easy>(curlMulti);
                easy->set_url(url);
                easy->set_sink(sink);
                return easy;
            }

//            auto easyIt = (*it).second.rbegin();
            auto easy = (*it).second.back();
//            easy->set_url(url);
            easy->set_sink(sink);
            (*it).second.pop_back();//.erase(easyIt);
//            curlEasyCacheMap.erase(it);
            return easy;
        };

    auto returnCurlEasy = [curlCache, &curlEasyCacheMutex, &curlEasyCacheMap](const string& url, const std::shared_ptr<curl::easy>& easy)
        {
            std::lock_guard<std::mutex> lock(curlEasyCacheMutex);
            if (curlCache)
                curlEasyCacheMap[url].push_back(easy);
        };

#endif

#ifdef AVRO_TESTING

    auto load = [](const char* filename) {
        std::ifstream ifs(filename);
        avro::ValidSchema result;
        avro::compileJsonSchema(ifs, result);
        return result;
    };


    avro::ValidSchema myrecordSchema = load("avro_test.json");

    auto isoDateTime = []() {
        time_t now;
        time(&now);
        char buf[sizeof "2011-10-08T07:07:09Z "];
        strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));
        // this will work too, if your compiler doesn't support %F or %T:
        //strftime(buf, sizeof buf, "%Y-%m-%dT%H:%M:%SZ", gmtime(&now));
        return string(&buf[0]);
    };

    static int counter = 0;
    socketIo.on("avro", [&](WebServer::ConnectionPtr conn, const json& data, SocketIo::CompleteCb completeCb)
        {
            logger->debug("socketIo.on 'avro'");

            myrecord myRecord;
            myRecord.uid = counter++;
            myRecord.somefield = isoDateTime();
            for (size_t i = 0; i < 5; ++i)
            {
                lvl2_record lvl2Record;
                lvl2Record.item1_lvl2 = intToString(counter++);// boost::lexical_cast<string>(counter++);

                for (size_t j = 0; j < 10; ++j)
                {
                    lvl3_record lvl3Record;
                    lvl3Record.item1_lvl3 = intToString(counter++); // boost::lexical_cast<string>(counter++);
                    lvl3Record.item2_lvl3 = "lvl3";
                    lvl2Record.item2_lvl2.push_back(lvl3Record);
                }

                myRecord.options.push_back(lvl2Record);
            }

//            std::auto_ptr<avro::OutputStream> out = avro::memoryOutputStream();
            std::ostringstream oss;
            std::auto_ptr<avro::OutputStream> out = avro::ostreamOutputStream(oss);

            avro::EncoderPtr e = avro::jsonEncoder(myrecordSchema);//binaryEncoder();
            e->init(*out);
            avro::encode(*e, myRecord);
            (*e).flush();

            logger->debug("avro stream size: {0}", oss.str().size());//, oss.str());

#ifdef CURL_ASIO_TESTING

            auto sink = std::make_shared<std::ostringstream>();// boost::make_shared<std::ostringstream>();
            string url = targetUrl;//"http://localhost:19998/avro";
            auto easy = getCurlEasy(url, sink);//std::make_shared<curl::easy>(curlMulti);

#if 1
            easy->async_perform([&, url, easy, sink, completeCb/*, cb = std::move(completeCb)*/](const boost::system::error_code& err) {

#if 1
                logger->debug("curl response: {0}", easy->get_reponse_code());
                logger->debug("sink {0}", sink->str().size());

                if (err)
                {
                    logger->error("Curl error: response_code:{0}, http_connectcode:{1} : {2}", easy->get_reponse_code(), easy->get_http_connectcode(), err.message());
                }

                returnCurlEasy(url, easy);
                if (completeCb)
                    completeCb(true);
#endif
            });
#else

            auto timer = std::make_shared<boost::asio::deadline_timer>(ioService);
            timer->expires_from_now(boost::posix_time::milliseconds(1));
            timer->async_wait([&, timer, completeCb](const boost::system::error_code& e)
                    {
                        if (!e)
                        {
                            logger->debug("timer");
                            if (completeCb)
                                completeCb(true);
                        }
                    });

//            if (completeCb)
//                completeCb(true);
#endif

#else
            if (completeCb)
                completeCb(true);
#endif

            return true;
        });
#endif

    std::size_t avroHttpCounter = 0;

    std::vector<std::shared_ptr<WebServer>> servers;
    for (const auto port : webserverPorts)
    {
        std::shared_ptr<WebServer> webServer = std::make_shared<WebServer>(ioService, port);

        webServer->addRoute(WebServer::RouteSpec::create("/somePath/")
                .setWebsocketMessageCb(webserverMsgCb)
                .setHttpRequestCb(httpReqCb));

#ifdef CURL_ASIO_TESTING
        webServer->addRoute(WebServer::RouteSpec::create("/avro")
                .addMethod("POST")
                .addMethod("GET")
                .setHttpRequestCb([&](WebServer::RequestPtr request, WebServer::ResponsePtr response)
                    {
//                        const WebServer::Request& req = *request;
                        auto req = *request;
                        auto resp = *response;
                        ++avroHttpCounter;
                        logger->debug("httpCb : route:{0}, remote:{1}, uri:{2}, method:{3}, msg:{4}", req.route, req.remote, req.uri, req.method, req.msg);
                        resp.setHttpStatus(websocketpp::http::status_code::ok);
                        return true;
                    }));
#endif
        WebServer::RouteSpec socketIoRoute = WebServer::RouteSpec::create(cfg().get<string>({"socketIo", "path"}, "/socket.io/"))
                .setWebsocketMessageCb([&](WebServer::RequestPtr request, WebServer::ResponsePtr response)
                    {
                        socketIo.handleMsg(request/*, response*/);
                        return false;
                    })
                .setWebsocketConnectCb([&](bool connected, WebServer::RequestPtr request)
                    {
                        auto req = *request;
                        if (connected)
                            socketIo.handleConnect(req.conn, req.remote, req.uri, req.method);
                        else
                            socketIo.handleDisconnect(req.conn);
                    });

        webServer->addRoute(socketIoRoute);

        servers.push_back(webServer);
    }

    boost::asio::deadline_timer statsTimer(ioService);
    std::function<void()> startStatsTimer;
    startStatsTimer = [&]()
    {
        statsTimer.expires_from_now(boost::posix_time::milliseconds(1000));
        statsTimer.async_wait([&](const boost::system::error_code& e)
            {
                if (!e)
                {
                    std::size_t msgs = socketIo.msgsHandled();
                    logger->info() << "msgs: " << socketIo.msgsHandled() << " clients: " << servers[0]->getConnectedClientCount() << "  avroCounter: " << avroHttpCounter;
#ifdef CURL_ASIO_TESTING
                    logger->info() << "curlEasyCacheMap size: " << curlEasyCacheMap.size() << " (" << (curlEasyCacheMap.empty() ? 0 : curlEasyCacheMap.begin()->second.size());
#endif

                    if (exitAfterMsgCount > 0 && static_cast<int>(msgs) >= exitAfterMsgCount)
                        ioService.stop();

                    startStatsTimer();
                }
            });
    };

    startStatsTimer();

//    ioService.run();
#if 1
    // Create a pool of threads to run all of the io_services.
    std::vector<std::thread> threads;
    for (int i = 0; i < webWorkerThreadCount; ++i)
    {
        threads.push_back(std::thread(std::bind(
            static_cast<std::size_t(boost::asio::io_service::*)()>(&boost::asio::io_service::run), &ioService)));
    }

    // Wait for all threads in the pool to exit.
    for (std::size_t i = 0; i < threads.size(); ++i)
      threads[i].join();
#endif

    return 0;
}

void printUsage(const string& programName)
{
    cout << "Usage: " << programName << " [options]" << endl
         << "  -c <configuration-file.json>     : Path to configuration JSON file." << endl
         << "  -p <webserverPort>               : Websocket port to listen on" << endl
         << "  -l <log-level>                   : Default 'info'. 'trace', 'debug', 'info', 'notice', 'warning', 'error', 'critical', 'alert', 'emerg', 'off'" << endl
         << "  -w <worker-thread-count>         : Number of worker threads to service requests. Default = 10." << endl;
}
