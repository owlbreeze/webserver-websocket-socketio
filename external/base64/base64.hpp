/**
 * Copyright (C) 2004-2008 Rene Nyffenegger
 *
 * This source code is provided 'as-is', without any express or implied
 * warranty. In no event will the author be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this source code must not be misrepresented; you must not
 *    claim that you wrote the original source code. If you use this source code
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original source code.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * \author Rene Nyffenegger rene.nyffenegger@adp-gmbh.ch
 * \author Artur Troian troian.ap@gmail.com
 */

#pragma once

#include <iostream>
#include <string>
#include <vector>

namespace base64
{

    static const std::string& base64Chars()
    {
        static const std::string chars =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "abcdefghijklmnopqrstuvwxyz"
            "0123456789+/";
        return chars;
    }

    inline char base64Char(unsigned index)
    {
        static const std::string chars = base64Chars();

        return index < chars.size() ? chars[index] : 0;
    }

    inline bool isBase64(unsigned char c)
    {
		return (isalnum(c) || (c == '+') || (c == '/'));
	}

    inline std::string encode(const uint8_t* data, size_t length)
    {
        std::string b64;
        b64.reserve(length);

        int i = 0;
        int j = 0;
        int k = 0;
        uint8_t array_3[3];
        uint8_t array_4[4];

        while (length--)
        {
            array_3[i++] = data[k++];
            if (i == 3)
            {
                array_4[0] = (array_3[0] & 0xfc) >> 2;
                array_4[1] = ((array_3[0] & 0x03) << 4) + ((array_3[1] & 0xf0) >> 4);
                array_4[2] = ((array_3[1] & 0x0f) << 2) + ((array_3[2] & 0xc0) >> 6);
                array_4[3] = array_3[2] & 0x3f;

                for (i = 0; (i < 4); i++)
                {
                    b64 += base64Char(array_4[i]);
                }
                i = 0;
            }
        }

        if (i)
        {
            for (j = i; j < 3; j++)
            {
                array_3[j] = '\0';
            }

            array_4[0] = (array_3[0] & 0xfc) >> 2;
            array_4[1] = ((array_3[0] & 0x03) << 4) + ((array_3[1] & 0xf0) >> 4);
            array_4[2] = ((array_3[1] & 0x0f) << 2) + ((array_3[2] & 0xc0) >> 6);
            array_4[3] = array_3[2] & 0x3f;

            for (j = 0; (j < i + 1); j++)
            {
                b64 += base64Char(array_4[j]);
            }

            while ((i++ < 3))
            {
                b64 += '=';
            }
        }

        return b64;
    }

    inline std::string encode(const std::vector<uint8_t>& data)
    {
        return encode(data.data(), data.size());
    }

    inline std::string encode(const std::string &data)
    {
        return encode(static_cast<const uint8_t*>(static_cast<const void*>(data.data())), data.size());
    }

	/**
     * \brief   Decode base64 string into array.
	 *
	 * \param[in]  stream: base64 stream
	 *
	 * \return
	 */
    inline std::string decode(const std::string &stream)
    {
        static const std::string chars = base64Chars();

		int in_len = stream.size();
		int i = 0;
		int j = 0;
		int in_ = 0;
		uint8_t array_4[4];
		uint8_t array_3[3];
        std::string ret;

        while (in_len-- && (stream[in_] != '=') && isBase64(stream[in_]))
        {
			array_4[i++] = stream[in_];
			in_++;
            if (i == 4)
            {
                for (i = 0; i < 4; i++)
                {
                    array_4[i] = chars.find(array_4[i]);
				}

				array_3[0] = (array_4[0] << 2) + ((array_4[1] & 0x30) >> 4);
				array_3[1] = ((array_4[1] & 0xf) << 4) + ((array_4[2] & 0x3c) >> 2);
				array_3[2] = ((array_4[2] & 0x3) << 6) + array_4[3];

                for (i = 0; (i < 3); i++)
                {
					ret.push_back(array_3[i]);
				}

				i = 0;
			}
		}

        if (i)
        {
            for (j = i; j < 4; j++)
            {
				array_4[j] = 0;
			}

            for (j = 0; j < 4; j++)
            {
                array_4[j] = chars.find(array_4[j]);
			}

			array_3[0] = (array_4[0] << 2) + ((array_4[1] & 0x30) >> 4);
			array_3[1] = ((array_4[1] & 0xf) << 4) + ((array_4[2] & 0x3c) >> 2);
			array_3[2] = ((array_4[2] & 0x3) << 6) + array_4[3];

            for (j = 0; (j < i - 1); j++)
            {
				ret.push_back(array_3[j]);
			}
		}

		return ret;
	}


    std::string urlEncode(const uint8_t *buf, size_t len)
    {
        std::string encodedString;
        encodedString.reserve(len);

        for (size_t i = 0; i < len; ++i)
        {
            switch (buf[i])
            {
                case '+':
                    encodedString.push_back('-');
                    break;
                case '/':
                    encodedString.push_back('_');
                    break;
                case '=':
                    // omit pad char
                    continue;
                default:
                    encodedString.push_back(buf[i]);
                    break;
            }
        }

        return encodedString;
    }

    std::string urlEncode(const std::string& str)
    {
        return urlEncode((uint8_t *)str.data(), str.size());
    }

    std::string urlDecode(const uint8_t *buf, size_t len)
    {
        std::string decodedString;

        for (size_t i = 0; i < len; ++i)
        {
            switch (buf[i])
            {
                case '-':
                    decodedString.push_back('+');
                    break;
                case '_':
                    decodedString.push_back('/');
                    break;
                default:
                    decodedString.push_back(buf[i]);
                    break;
            }
        }

        switch (len % 4)
        {
            case 0:
                break;
            case 2:
                decodedString += "==";
                break;
            case 3:
                decodedString += "=";
                break;
            default:
                throw std::runtime_error("Illegal base64url string");
                break;
        }

        return decodedString;
    }

    std::string urlDecode(const std::string& data)
    {
        return urlDecode(static_cast<const uint8_t*>(static_cast<const void*>(data.data())), data.size());
    }


} // namespace base64
